/*******************************************************************************
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 ******************************************************************************/
package au.gov.vic.vec;

/**
 * SignatureCheckException could occur when checking the signatures of files in the bundle zip
 * 
 */
public class SignatureCheckException extends Exception {

  /**
   * Required for inherited serialisation.
   */
  private static final long serialVersionUID = -1901601738572639949L;
  /**
   * Constructs a new exception with {@code null} as its detail message.
   */
  public SignatureCheckException() {

  }

  /**
   * Constructs a new exception with the specified detail message.
   * 
   * @param detailMessage
   *          the detail message.
   */
  public SignatureCheckException(String detailMessage) {
    super(detailMessage);

  }

  /**
   * Constructs a new exception with the specified detail message and cause.
   * <p>
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically incorporated in this exception's detail
   * message.
   * 
   * @param detailMessage
   *          the detail message.
   * @param throwable
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public SignatureCheckException(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);

  }

  /**
   * Constructs a new exception with the specified cause and a detail message.
   * 
   * @param throwable
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public SignatureCheckException(Throwable throwable) {
    super(throwable);

  }

}
