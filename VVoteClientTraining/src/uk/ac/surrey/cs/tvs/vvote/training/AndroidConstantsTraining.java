/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.training;

import uk.ac.surrey.cs.tvs.vvote.client.AndroidConstants;
import android.os.Environment;

/**
 * Class containing static constants for referencing the various config values in the android config file
 * 
 * @author Chris Culnane
 * 
 */
public class AndroidConstantsTraining extends AndroidConstants {

  /**
   * Paths constants for location of various config files
   * 
   * @author Chris Culnane
   * 
   */
  public static class Paths {

    /**
     * App folder path (sdcard/trainingmode)
     */
    public static final String APP_FOLDER        = Environment.getExternalStorageDirectory().getAbsolutePath() + "/trainingmode/";

    /**
     * Location of config file - inside APP_FOLDER config.json
     */
    public static final String CONFIG_FILE       = APP_FOLDER + "config.json";

    /**
     * Location of client config file - inside APP_FOLDER client.conf
     */
    public static final String CLIENT_CONFIG     = APP_FOLDER + "client.conf";

    /**
     * Location of EVM Client config file - inside APP_FOLDER evmclient.conf
     */
    public static final String EVM_CLIENT_CONFIG = APP_FOLDER + "evmclient.conf";

    /**
     * Location of district config file - inside APP_FOLDER districtconf.json
     */
    public static final String DISTRICT_CONFIG   = APP_FOLDER + "districtconf.json";

  }
}
