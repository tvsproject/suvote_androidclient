/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.view.View;

/**
 * This is the activity that provides the UI for starting and stopping the VVoteService. In time this will provide further
 * diagnostic and status information. Currently it just provides and start and stop button
 * 
 * @author Chris Culnane
 * 
 */
public class VVoteManager extends ServiceManager {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(VVoteManager.class);

  /**
   * Handles the onClick of the button
   */
  @Override
  public void onClick(View src) {
    switch (src.getId()) {
      case R.id.buttonStart:
        logger.info("Start button pressed");
        this.startService(new Intent(this, VVoteService.class));
        break;
      case R.id.buttonStop:
        logger.info("Stop button pressed");
        this.stopService(new Intent(this, VVoteService.class));
        break;
      default:
        logger.info("Unknown button pressed");
    }
  }

}
