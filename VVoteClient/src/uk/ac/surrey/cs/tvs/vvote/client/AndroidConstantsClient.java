/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import android.os.Environment;

/**
 * Class containing static constants for referencing the various config values in the android config file
 * 
 * @author Chris Culnane
 * 
 */
public class AndroidConstantsClient extends AndroidConstants {

  /**
   * Paths constants for location of various config files
   * 
   * @author Chris Culnane
   * 
   */
  public static class Paths extends AndroidConstants.Paths {

    /**
     * App folder location
     */
    public static final String APP_FOLDER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/vvoteclient/";

  }
}
