/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.client.ClientType;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

/**
 * BroadcastWorker receives intents and performs the requested action. This is the class that provides the adb intent interface for
 * setting up a client without using the clientUI.
 * 
 * TODO There are string literals that could be replaced by constants
 * 
 * @author Chris Culnane
 * 
 */
public class BroadcastWorker implements Runnable, ProgressListener {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(BroadcastWorker.class);

  /**
   * TAG to use in adb logcat
   */
  private static final String TAG    = "uk.ac.surrey.cs.BroadcastWorker";

  /**
   * The intent object that the worker is using
   */
  private Intent              intent;

  /**
   * Current operation being undertaken
   */
  private String              currentOp;

  /**
   * The underlying Client object that the operations are being called on
   */
  private Client              client;

  /**
   * Creates a new BroadcastWorker with the specified Client and Intent
   * 
   * @param client
   *          Client to operate on
   * @param intent
   *          Intent that made the call
   */
  public BroadcastWorker(Client client, Intent intent) {
    this.client = client;
    this.intent = intent;

  }

  /**
   * Performs the actual processing
   */
  @Override
  public void run() {
    String action = this.intent.getAction();
    logger.info("Action: {} received via broadcast", action);
    if (action.equals(ClientInit.ACTION_GET_STATUS)) {
      if (this.client.isInitialised()) {
        Log.i(TAG, "Initialised");
      }
      else {
        Log.i(TAG, this.client.getNextTask());
      }
    }
    else if (action.equals(ClientInit.ACTION_CREATE_SIGNING_KEYS)) {
      if (this.client.getStatus() == 0) {
        this.currentOp = "GenSigningKey";
        this.client.addProgressListener(this);
        try {
          this.client.generateSigningKey();
        }
        catch (ClientException e) {
          logger.error("Exception generating signing keys", e);
        }
        this.client.removeProgressListener(this);
        try {
          IOUtils.writeStringToFile(this.client.getJSONPublicKey(), Environment.getExternalStorageDirectory().getPath()
              + "/vvoteclient/" + this.client.getClientConf().getStringParameter(ConfigFiles.ClientConfig.ID) + "_Public"
              + ConfigFiles.ClientConfig.SIGNING_KEY_FILENAME);
        }
        catch (IOException e) {
          logger.error("Exception saving signing keys", e);
        }
        catch (ClientException e) {
          logger.error("Exception saving signing keys", e);
        }
      }
      else {
        Log.i(TAG, "Error, Invalid operation at this time");
      }
    }
    else if (action.equals(ClientInit.ACTION_CREATE_CRYPTO_KEYS)) {
      if (this.client.getType() == ClientType.VPS && this.client.getStatus() == 2) {
        this.currentOp = "GenCryptoKey";
        this.client.addProgressListener(this);
        try {
          this.client.generateCryptoKeyPair();
        }
        catch (ClientException e) {
          logger.error("Exception generating crypto keys", e);
          Log.i(TAG, "Error, " + e.getMessage());
        }
        this.client.removeProgressListener(this);
        Log.i(TAG, this.currentOp + "," + "Complete");
      }
      else {
        Log.i(TAG, "Error, Invalid operation at this time");
      }
    }
    else if (action.equals(ClientInit.ACTION_IMPORT_CSR)) {
      this.currentOp = "ImportCSR";
      Log.i(TAG, this.currentOp + "," + this.intent.getStringExtra("SignedCSRPath"));
      try {
        this.client.importSSLCertificate(this.intent.getStringExtra("SignedCSRPath"));
        Log.i(TAG, this.currentOp + "," + "Complete");
      }
      catch (ClientException e) {
        logger.error("Exception importing CSR", e);
        Log.i(TAG, "Error," + e.getMessage());
      }
    }
    else if (action.equals(ClientInit.ACTION_GEN_BALLOTS)) {
      if (this.client.getType() == ClientType.VPS && this.client.getStatus() == 4) {
        this.currentOp = "GenBallots";
        this.client.addProgressListener(this);
        try {
          this.client.generateBallots();
          Log.i(TAG, this.currentOp + "," + "Complete");
        }
        catch (ClientException e) {
          logger.error("Exception generating ballots", e);
          Log.i(TAG, "Error, " + e.getMessage());
        }
        this.client.removeProgressListener(this);
      }
      else {
        Log.i(TAG, "Error, Invalid operation at this time");
      }
    }
  }

  /**
   * Updates the progress by outputting a value to logcat
   */
  @Override
  public void updateProgress(int progress) {
    Log.i(TAG, this.currentOp + "," + progress);
  }
}
