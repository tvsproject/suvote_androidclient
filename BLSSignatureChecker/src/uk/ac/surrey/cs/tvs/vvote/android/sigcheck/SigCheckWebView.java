/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.android.sigcheck;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Creates a SigCheckWebView activity that consists of a single WebView that fills the screen and interacts with the native Android
 * methods for reading the barcode and checking the BLS Signature. This is a similar approach to PhoneGap/Cordova - we have created
 * our own variant as a result of problems getting PhoneGap to even run
 * 
 * 
 */
public class SigCheckWebView extends Activity {

  /**
   * WebView that presents the entire UI
   */
  private WebView webView;

  /**
   * Called when the BarcodeScanner Intent returns. This gets called even if the barcode scanner has failed or been cancelled. Hence
   * we check the result and then pass the relevant value to the JavaScript
   * 
   * @param requestCode
   *          int requestcode of the intent
   * @param resultCode
   *          int value indicating what type of result we have received from the BarcodeScanner
   * @param intent
   *          Intent that was called
   */
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    if (scanResult != null) {
      this.webView.loadUrl("javascript:barcodeResult(\"" + scanResult.getContents() + "\");");
    }
    else {
      this.webView.loadUrl("javascript:barcodeResult();");

    }
  }

  /**
   * Creates a new instace of the SigCheckWebView activity, loading the UI HTML and pre-loading the CurveParams, which is a slow
   * task on Android
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.activity_sig_check_web_view);

    // Get the WebView
    this.webView = (WebView) this.findViewById(R.id.webViewContent);

    // Create a JavaScript Interface object to interact with the WebView
    JavaScriptInterface jsInterface = new JavaScriptInterface(this, this.webView);
    this.webView.getSettings().setJavaScriptEnabled(true);
    this.webView.addJavascriptInterface(jsInterface, "JSInterface");

    // Load the initial UI
    this.webView.loadUrl("file:///android_asset/checker.html");

    // Create a ProgressDialog to load the CurveParams with
    ProgressDialog pd = new ProgressDialog(this);
    pd.setMessage("Loading Curve Params...\n(This may take several minutes)");
    pd.setCancelable(false);
    pd.setIndeterminate(true);
    ParamLoader params = new ParamLoader(pd);

    // Loads the actual params
    params.execute();

  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
  }

}
