/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.android.sigcheck;

import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.webkit.WebView;

/**
 * AsyncTask for checking a signature. This is required because the pure Java implementation of PBC is extremely slow on Android
 * devices. It takes up to a minute to create and check the pairing. As such, we run a progress dialog and the actual check on a
 * Separate thread to stop the underlying OS.
 * 
 * @author Chris Culnane
 * 
 */
public class SignatureChecker extends AsyncTask<Void, Void, Boolean> {

  /**
   * ProgressDialog to display whilst we check the signature
   */
  private ProgressDialog pd;
  /**
   * TVSSignature object that has been initialised with the correct key and message data
   */
  private TVSSignature   tvsSig;

  /**
   * Byte array of the actual signature
   */
  private byte[]         sig;

  /**
   * WebView for the app, we use this to call a JavaScript method to notify the UI of the result of the check
   */
  private WebView        webView;

  /**
   * Constructs a new SignatureChecker AsyncTask with the relevant ProgressDialog, TVSSignature, Byte Array signatures and WebView
   * UI
   * 
   * @param pd
   *          ProgressDialog to display
   * @param tvsSig
   *          TVSSignature object initialised with appropriate data
   * @param sig
   *          byte array of actual signature
   * @param webView
   *          WebView to submit result to
   */
  public SignatureChecker(ProgressDialog pd, TVSSignature tvsSig, byte[] sig, WebView webView) {
    this.pd = pd;
    this.tvsSig = tvsSig;
    this.sig = sig;
    this.webView = webView;
  }

  /**
   * Performs the actual operation in the background
   */
  @Override
  protected Boolean doInBackground(Void... arg0) {
    try {
      return this.tvsSig.verify(this.sig);
    }
    catch (TVSSignatureException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Closes the ProgressDialog and calls the JavaScript method to notify the front-end of the result
   */
  @Override
  protected void onPostExecute(Boolean result) {
    super.onPostExecute(result);
    this.pd.dismiss();
    this.webView.loadUrl("javascript:sigCheckResult(" + result + ");");
  }

  /**
   * Shows the progress dialog before starting to execute
   */
  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    this.pd.show();
  }

}
