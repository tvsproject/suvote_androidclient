/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;

/**
 * Listener to handle the floating notification touches and interactions
 * 
 * @author Chris Culnane
 * 
 */
public class FloatingNotificationListener implements OnTouchListener {

  /**
   * Layout params
   */
  private LayoutParams  params;
  /**
   * int with the initialX location
   */
  private int           initialX;
  /**
   * int with the initialY location
   */
  private int           initialY;

  /**
   * initial touch X location
   */
  private float         initialTouchX;
  
  /**
   * initial touch y location
   */
  private float         initialTouchY;
  /**
   * ImageView containing the actual notification icon
   */
  private ImageView     notification;

  /**
   * WindowManger to use for showing and hiding notifications
   */
  private WindowManager windowManager;

  /**
   * boolean to record if the location has moved
   */
  private boolean       hasMoved = false;

  /**
   * Creates a new FloatingNotificationListener using the specified WindowManger and LayoutParams
   * 
   * @param windowManager
   *          WindowManger to use to show notifications/dialogs
   * @param params
   *          LayoutParams for FloatingNotification
   * @param notification
   *          ImageView that holds the actual overlay icon
   */
  public FloatingNotificationListener(WindowManager windowManager, LayoutParams params, ImageView notification) {
    this.params = params;
    this.windowManager = windowManager;
    this.notification = notification;
  }

  /**
   * Handles the moving of the notification
   */
  @Override
  public boolean onTouch(View v, MotionEvent event) {
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        hasMoved = false;
        initialX = params.x;
        initialY = params.y;
        initialTouchX = event.getRawX();
        initialTouchY = event.getRawY();
        break;
      case MotionEvent.ACTION_UP:
        break;
      case MotionEvent.ACTION_MOVE:
        if (event.getRawX() != initialTouchX || event.getRawY() != initialTouchY) {
          hasMoved = true;
        }
        params.x = initialX + (int) (event.getRawX() - initialTouchX);
        params.y = initialY + (int) (event.getRawY() - initialTouchY);
        windowManager.updateViewLayout(notification, params);
        break;
    }
    return false;
  }

  /**
   * Checks if the touch should be ignored - for example, when moving the notification we don't want to show the dialog on touch
   * release
   * 
   * @return boolean true if the touch should be ignored, false if not
   */
  public boolean shouldIgnore() {
    if (hasMoved) {
      hasMoved = false;
      return true;
    }
    else {
      return false;
    }
  }

}
