/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * BroadcastReceiver to receive notifications of changes to the plugged in status of the tablet.
 * 
 * Note: This is currently not used. Old code is left commented out incase we decide to reactivate it and take this approach
 * 
 * @author Chris Culnane
 * 
 */
public class PowerConnectionReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    // context.getApplicationContext().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    // int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
    // int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
    // BatteryData.getInstance().updateChargeInfo(status, chargePlug);
  }

}
