/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

/**
 * Simple interface to use with the BatteryData singleton to receive battery status updates
 * 
 * @author Chris Culnane
 * 
 */
public interface BatteryStatusListener {

  /**
   * Called to update charge information, this will be passed on to any listeners. The two values are the charge and the plug status
   * 
   * @param status
   *          int value representing the state of the battery
   * @param plug
   *          int value representing the plug status of battery (AC, USB, etc)
   */
  public void batteryUpdate(int status, int plug);
}
