/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

import java.util.ArrayList;

/**
 * Battery data provides a singleton instance for registering and receiving battery status updates. By having a singleton instance
 * there is only one point needed for registering interest in it and a single class can then register for the intent and pass it on
 * to all other interested parties.
 * 
 * NOTE: This is currently not used - we currently use an inner class
 * 
 * @author Chris Culnane
 * 
 */
public class BatteryData {

  /**
   * ArrayList to hold the listeners
   */
  private ArrayList<BatteryStatusListener> listeners = new ArrayList<BatteryStatusListener>();

  /**
   * Private static instance of the BatteryData singleton
   */
  private static final BatteryData         _battData = new BatteryData();

  /**
   * Gets an instance of the BatteryData class
   * 
   * @return
   */
  public static BatteryData getInstance() {
    return _battData;
  }

  /**
   * Private default constructor to prevent this being constructed outside of the getInstance
   */
  private BatteryData() {
  }

  /**
   * Adds a listener to the singleton
   * 
   * @param listener
   *          BatteryStatusListener to be added
   */
  public void addListener(BatteryStatusListener listener) {
    this.listeners.add(listener);
  }

  /**
   * Removes a listener from the singleton
   * 
   * @param listener
   *          BatteryStatusListener to be removed
   */
  public void removeListener(BatteryStatusListener listener) {
    this.listeners.remove(listener);
  }

  /**
   * Called to update charge information, this will be passed on to any listeners. The two values are the charge and the plug status
   * 
   * @param status
   *          int value representing the state of the battery
   * @param plug
   *          int value representing the plug status of battery (AC, USB, etc)
   */
  public void updateChargeInfo(int status, int plug) {
    for (BatteryStatusListener listener : this.listeners) {
      listener.batteryUpdate(status, plug);
    }
  }

}
