/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vvote.json.JSONException;
import org.vvote.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import au.gov.vic.vec.DownloadBundleService;

/**
 * VECHomeScreen provides a custom Home screen launcher for the android tablet. This manages the starting/restarting of the
 * underlying services, as well as managing the DownloadBundle service and the removal/disabling of trainingmode once a bundle has
 * been downloaded. Additionally, it locks down the tablets to remove the drawers and action bar from functioning to prevent users
 * returning to the home screen or accessing settings.
 * 
 * @author Chris Culnane
 * 
 */
public class VECHome extends Activity implements OnClickListener, BatteryStatusListener {

  /**
   * When a drawable is attached to a View, the View gives the Drawable its dimensions by calling Drawable.setBounds(). In this
   * application, the View that draws the wallpaper has the same size as the screen. However, the wallpaper might be larger that the
   * screen which means it will be automatically stretched. Because stretching a bitmap while drawing it is very expensive, we use a
   * ClippedDrawable instead. This drawable simply draws another wallpaper but makes sure it is not stretched by always giving it
   * its intrinsic dimensions. If the wallpaper is larger than the screen, it will simply get clipped but it won't impact
   * performance.
   */
  private class ClippedDrawable extends Drawable {

    /**
     * Wallpaper to display
     */
    private final Drawable mWallpaper;

    /**
     * Constructor to create a ClippedDrawable from the wallpaper Drawable
     * 
     * @param wallpaper
     *          Drawable containing the wallpaper
     */
    public ClippedDrawable(Drawable wallpaper) {
      this.mWallpaper = wallpaper;
    }

    /**
     * Draws the wall paper onto the specified canvas
     * 
     * @param canvas
     *          Canvas to draw the wallpaper on
     */
    @Override
    public void draw(Canvas canvas) {
      this.mWallpaper.draw(canvas);
    }

    /**
     * Gets the opactiy of the wallpaper
     * 
     * @return int opacity of the wallpaper
     */
    @Override
    public int getOpacity() {
      return this.mWallpaper.getOpacity();
    }

    /**
     * Sets the alpha value of the wallpaper
     * 
     * @param alpha
     *          int alpha value
     */
    @Override
    public void setAlpha(int alpha) {
      this.mWallpaper.setAlpha(alpha);
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
      super.setBounds(left, top, right, bottom);
      // Ensure the wallpaper is as large as it really is, to avoid stretching it
      // at drawing time
      this.mWallpaper.setBounds(left, top, left + this.mWallpaper.getIntrinsicWidth(), top + this.mWallpaper.getIntrinsicHeight());
    }

    /**
     * Sets the colout filter of the wallpaper
     * 
     * @param cf
     *          ColorFilter to apply to the wallpaper
     */
    @Override
    public void setColorFilter(ColorFilter cf) {
      this.mWallpaper.setColorFilter(cf);
    }
  }

  /**
   * Receives DownloadBundleReceiver updates
   * 
   * @author Chris Culnane
   * 
   */
  public class DownloadBundleReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
      String responseType = intent.getStringExtra(DownloadBundleService.RESPONSE_STRING);
      String responseMsg = intent.getStringExtra(DownloadBundleService.RESPONSE_MESSAGE);
      if (responseType.equalsIgnoreCase(DownloadBundleService.REQUEST_STATUS)) {
        ((TextView) VECHome.this.findViewById(R.id.lastStatus)).setText("Status:" + responseMsg);
        if (responseMsg.equals("SUCCESS")) {
          VECHome.this.removeTrainingMode();
          VECHome.this.cleanUpBrowser();
          ((CheckBox) VECHome.this.findViewById(R.id.bundleCheckbox)).setChecked(true);
          ((Button) VECHome.this.findViewById(R.id.forceBundleCheck)).setClickable(false);
        }
      }
      else if (responseType.equalsIgnoreCase(DownloadBundleService.REQUEST_NEXT_SCHEDULED)) {
        ((TextView) VECHome.this.findViewById(R.id.nextScheduled)).setText("Next Check:" + responseMsg);
      }
      else if (responseType.equalsIgnoreCase(DownloadBundleService.REQUEST_LAST_DOWNLOAD)) {
        ((TextView) VECHome.this.findViewById(R.id.lastCheckText)).setText("Last Check:" + responseMsg);
      }
      logger.info("Received DownloadBundle update {},{}", responseMsg, responseType);
    }
  }

  class PackageDeleteObserver extends IPackageDeleteObserver.Stub {

    public PackageDeleteObserver() {

    }

    @Override
    public void packageDeleted(String packageName, int returnCode) throws RemoteException {
      logger.info("{} has been deleted", packageName);

      VECHome.this.runOnUiThread(new Runnable() {

        @Override
        public void run() {
          VECHome.this.restartService(true);
          ((TextView) VECHome.this.findViewById(R.id.mode)).setText(Html
              .fromHtml("Current Mode:\t<font color=\"#333333\">Live</font>"));
          VECHome.this.updateSystemStatus();
          //
        }
      });

    }
  }

  /**
   * Receives status update from the currently running vVoteService
   * 
   * @author Chris Culnane
   * 
   */
  public class VVoteServiceReceiver extends BroadcastReceiver {

    /**
     * Static variable for Request string in intent
     */
    private static final String RESPONSE_STRING               = "vVoteServiceRequestType";

    /**
     * Response field value
     */
    private static final String RESPONSE_MESSAGE              = "vVoteServiceRequestMsg";
    /**
     * Requests a general status update
     */
    private static final String REQUEST_STATUS                = "status";
    /**
     * indicates whether the webcam is connected
     */
    private static final String RESP_WEBCAM_CONNECTED         = "webcamConnected";

    /**
     * indicates if the printer is ready
     */
    private static final String RESP_PRINTER_CONNECTED        = "printerReady";

    /**
     * indicates if the ClientUI (initialisation) server is running
     */
    private static final String RESP_CLIENTUI_RUNNING         = "clientUI";

    /**
     * indicates if the POD proxy is running
     */
    private static final String RESP_PROXY_SERVER_RUNNING     = "proxyServer";

    /**
     * indicates if the EVM proxy is running
     */
    private static final String RESP_PROXY_SERVER_EVM_RUNNING = "proxyServerEVM";

    /**
     * Receives the vVoteService broadcastsa and processes them accordingly - for example, displaying status information
     */
    @Override
    public void onReceive(Context context, Intent intent) {

      String responseType = intent.getStringExtra(RESPONSE_STRING);
      String responseMsg = intent.getStringExtra(RESPONSE_MESSAGE);
      if (responseType.equalsIgnoreCase(REQUEST_STATUS)) {
        try {
          logger.info("Received response from vVoteSerice: {}", responseMsg);
          JSONObject resp = new JSONObject(responseMsg);
          VECHome.this.proxyRunning = resp.getBoolean(RESP_PROXY_SERVER_RUNNING);
          VECHome.this.proxyEVMRunning = resp.getBoolean(RESP_PROXY_SERVER_EVM_RUNNING);
          if (resp.getBoolean(RESP_CLIENTUI_RUNNING)) {
            ((Button) VECHome.this.findViewById(R.id.launchClientUI)).setVisibility(View.VISIBLE);
          }
          else {
            ((Button) VECHome.this.findViewById(R.id.launchClientUI)).setVisibility(View.GONE);
          }
          ((CheckBox) VECHome.this.findViewById(R.id.clientUICheckbox)).setChecked(resp.getBoolean(RESP_CLIENTUI_RUNNING));
          ((CheckBox) VECHome.this.findViewById(R.id.evmProxyCheckbox)).setChecked(resp.getBoolean(RESP_PROXY_SERVER_EVM_RUNNING));
          ((CheckBox) VECHome.this.findViewById(R.id.vpsProxyCheckbox)).setChecked(resp.getBoolean(RESP_PROXY_SERVER_RUNNING));
          ((CheckBox) VECHome.this.findViewById(R.id.printerCheckbox)).setChecked(resp.getBoolean(RESP_PRINTER_CONNECTED));
          ((CheckBox) VECHome.this.findViewById(R.id.webcamCheckbox)).setChecked(resp.getBoolean(RESP_WEBCAM_CONNECTED));
        }
        catch (JSONException e) {
          logger.error("Exception passing vVoteService response", e);
        }
      }

    }
  }

  /**
   * Logger
   */
  private static final Logger    logger                = LoggerFactory.getLogger(VECHome.class);
  /**
   * float containing current battery percent
   */
  private float                  battPerc              = 0.0f;

  /**
   * String to hold the currently used client service (training or vecclient)
   */
  private String                 clientService;

  /**
   * String to hold the package name of the currently running client service
   */
  private String                 clientServicePackage;

  /**
   * Boolean that records whether the proxy (VPS) is running
   */
  private boolean                proxyRunning          = false;

  /**
   * Boolean that records whether the EVM is running
   */
  private boolean                proxyEVMRunning       = false;

  /**
   * VVoteServiceReceiver for training mode
   */
  private VVoteServiceReceiver   vVoteTrainingReceiver = null;

  /**
   * VVoteServiceReceiver for the actual vVoteClient
   */
  private VVoteServiceReceiver   vVoteReceiver         = null;

  /**
   * Location of the staging configuration file
   */
  private static final String    STAGING_CONFIG_FILE   = Environment.getExternalStorageDirectory().getPath()
                                                           + "/vVoteClient/vvwww/stagingconfig.json";
  /**
   * String constant for retrieving launchURL from stagingconfig
   */
  private static final String    LAUNCH_URL            = "launchURL";
  /**
   * String constant for the Heartbeat URL from stagingconfig
   */
  private static final String    HEARTBEAT_URL         = "heartbeatURL";
  /**
   * String constant for the connectivity check URL from stagingconfig
   */
  private static final String    CONN_CHECK_URL        = "connCheckURL";

  /**
   * String constant for the F5 launch URL from stagingconfig
   */
  private static final String    F5_LAUNCH_URL         = "f5LaunchURL";

  /**
   * BroadcastReceiver for battery level changes
   */
  private BroadcastReceiver      batteryLevelReceiver;

  /**
   * BroadcastReceiver for headset unplug
   */
  private BroadcastReceiver      headsetReceiver;

  /**
   * DownloadBundleReceiver for notification of bundle downloads
   */
  private DownloadBundleReceiver receiver;

  /**
   * BroadcastReceiver for connectivity events
   */
  private BroadcastReceiver      connectivityReceiver;
  /**
   * String that holds stagingconfig launchURL or null if not set
   */
  private String                 launchURL             = null;
  /**
   * String that holds stagingconfig connCheckURL or null if not set
   */
  private String                 connCheckURL          = null;
  /**
   * String that holds stagingconfig heartbeatURL or null if not set
   */
  private String                 heartbeatURL          = null;
  /**
   * String that holds stagingconfig F5 Launch URL or null if not set
   */
  private String                 f5LaunchURL           = null;

  @Override
  public void batteryUpdate(int status, int plug) {
    this.updateChargeInfo(status, plug);
  }

  /**
   * Checks the battery level and calculates the percentage and displays it
   */
  public void checkBatteryLevel() {
    IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    Intent batteryStatus = this.getApplicationContext().registerReceiver(null, ifilter);
    int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
    this.battPerc = (level / (float) scale) * 100;
    ProgressBar currentChargePB = (ProgressBar) this.findViewById(R.id.currentCharge);
    currentChargePB.setProgress(Math.round(this.battPerc));
    ((TextView) this.findViewById(R.id.currentChargeText)).setText("Current Charge:" + String.valueOf(this.battPerc));

    int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
    int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
    this.updateChargeInfo(status, chargePlug);
  }

  /**
   * Method to close down and cleanup the browser. This was needed for the ClientUI page, which sometimes would keep the websocket
   * open and block further pages from accessing the clientUI.
   */
  private void cleanUpBrowser() {
    logger.info("Shutting down browser");
    ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
    List<RunningAppProcessInfo> processes = am.getRunningAppProcesses();
    if (processes != null) {
      for (int i = 0; i < processes.size(); i++) {

        RunningAppProcessInfo temp = processes.get(i);
        String pName = temp.processName;
        if (pName.equalsIgnoreCase("org.mozilla.firefox")) {
          try {
            logger.info("Trying to kill org.mozilla.firefox");
            Method forceStopPackage = am.getClass().getDeclaredMethod("forceStopPackage", String.class);
            forceStopPackage.setAccessible(true);
            forceStopPackage.invoke(am, "org.mozilla.firefox");

            // android.os.Process.killProcess(temp.pid);
            break;
          }
          catch (NoSuchMethodException e) {
            logger.error("Exception whilst trying to close Firefox", e);
          }
          catch (IllegalArgumentException e) {
            logger.error("Exception whilst trying to close Firefox", e);
          }
          catch (IllegalAccessException e) {
            logger.error("Exception whilst trying to close Firefox", e);
          }
          catch (InvocationTargetException e) {
            logger.error("Exception whilst trying to close Firefox", e);
          }

        }
      }
    }
  }

  /**
   * Disables the status bar - this requires root permissions
   */
  public void disableStatusBar() {
    try {
      View decorView = getWindow().getDecorView();
      logger.info("Hiding status bar and setting click listeners for buttons");
      // Hide the status bar.
      int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
      decorView.setSystemUiVisibility(uiOptions);
      logger.info("Attempting to disable the status bar");
      Object service = this.getSystemService("statusbar");
      Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
      Method expand = statusbarManager.getMethod("disable", Integer.TYPE);
      expand.invoke(service, statusbarManager.getField("DISABLE_MASK").getInt(null));
      logger.info("No exceptions thrown - assume it has worked");
    }
    catch (Exception ex) {
      logger.warn("There was an exception when trying to disable the status bar - is the app running as root?", ex);
    }
  }

  /**
   * Checks if trainingmode is installed. If trainingmode is installed it will run trainingmode instead of the main client app
   * 
   * @return boolean true if it is installed, false if not
   */
  private boolean isTrainingModeInstalled() {

    final PackageManager pm = this.getPackageManager();
    // get a list of installed apps.
    List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

    for (ApplicationInfo packageInfo : packages) {
      if (packageInfo.packageName.equalsIgnoreCase("uk.ac.surrey.cs.tvs.vvote.training")) {
        logger.info("Training mode found in installed apps, showing launch training mode button and hiding launch vVote button");
        ((Button) this.findViewById(R.id.launchTraining)).setVisibility(View.VISIBLE);
        ((Button) this.findViewById(R.id.launchVVote)).setVisibility(View.GONE);
        this.clientService = "uk.ac.surrey.cs.tvs.vvote.training.VVoteTrainingService";
        this.clientServicePackage = "uk.ac.surrey.cs.tvs.vvote.training";

        return true;
      }
    }

    this.clientService = "uk.ac.surrey.cs.tvs.vvote.client.VVoteService";
    this.clientServicePackage = "uk.ac.surrey.cs.tvs.vvote.client";
    logger.info("Did not find training mode, will assume client is installed");
    ((Button) this.findViewById(R.id.launchTraining)).setVisibility(View.GONE);
    ((Button) this.findViewById(R.id.launchVVote)).setVisibility(View.VISIBLE);
    return false;
  }

  /**
   * Handles the button pressed on the VECHome screen
   * 
   * @param src
   *          View that capture the button press
   */
  @Override
  public void onClick(View src) {
    String url = "";

    switch (src.getId()) {
      case R.id.launchTraining:
        logger.info("Training mode launch requested");
        if (this.proxyRunning) {
          logger.info("Proxy is running, start VPS");
          url = "http://127.0.0.1:8090/vps.html";
        }
        else if (this.proxyEVMRunning) {
          logger.info("EVM is running, start EVM");
          url = "http://127.0.0.1:8060";
        }
        this.cleanUpBrowser();
        logger.info("Starting firefox");
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setComponent(new ComponentName("org.mozilla.firefox", "org.mozilla.firefox.App"));
        intent.setAction("org.mozilla.gecko.BOOKMARK");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("args", "--url=" + url);
        intent.setData(Uri.parse(url));
        this.disableStatusBar();
        this.startActivity(intent);
        break;
      case R.id.launchVVote:
        this.cleanUpBrowser();
        logger.info("vVote launch requested");
        if (this.launchURL != null) {
          logger.info("LaunchURL set, will use it: {}", this.launchURL);
          url = this.launchURL;
        }
        else if (this.proxyRunning) {
          logger.info("Proxy is running, start VPS");
          url = "http://127.0.0.1:8090/vps.html";
        }
        else if (this.proxyEVMRunning) {
          logger.info("EVM is running, start EVM");
          url = "http://127.0.0.1:8060/evm.html";
        }
        if (!this.checkConnectivity(url, false)) {
          showNetworkWarning();
        }
        // logger.info("Starting firefox");
        // intent = new Intent(Intent.ACTION_MAIN, null);
        // intent.addCategory(Intent.CATEGORY_LAUNCHER);
        // intent.setComponent(new ComponentName("org.mozilla.firefox", "org.mozilla.firefox.App"));
        // intent.setAction("org.mozilla.gecko.BOOKMARK");
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // intent.putExtra("args", "--url=" + url);
        // intent.setData(Uri.parse(url));
        // this.disableStatusBar();
        // this.startActivity(intent);
        break;
      case R.id.launchClientUI:
        this.cleanUpBrowser();
        logger.info("ClientUI launch requested - should use adb interface");
        url = "http://127.0.0.1:8081/";
        intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setComponent(new ComponentName("org.mozilla.firefox", "org.mozilla.firefox.App"));
        intent.setAction("org.mozilla.gecko.BOOKMARK");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("args", "--url=" + url);
        intent.setData(Uri.parse(url));
        this.disableStatusBar();
        this.startActivity(intent);
        break;
      case R.id.forceBundleCheck:

        logger.info("Force bundle check requested");
        Intent msgIntent = new Intent(VECHome.this, DownloadBundleService.class);
        msgIntent.putExtra(DownloadBundleService.REQUEST_STRING, DownloadBundleService.REQUEST_DOWNLOAD);
        this.startService(msgIntent);
        break;
      case R.id.restartService:
        logger.info("Restart current client service");
        this.restartService(true);
        break;
      case R.id.callibrateWebCam:
        logger.info("Calibrate WebCam launch requested");
        intent = new Intent();
        intent.setAction("uk.ac.surrey.cs.tvs.vvote.webcam.CALLIBRATE");
        intent.addCategory("android.intent.category.DEFAULT");
        this.startActivity(intent);
        break;
      case R.id.sendHeartbeat:
        if (!this.checkConnectivity(this.heartbeatURL, true)) {
          showNetworkWarning();
        }
        break;
      case R.id.connectivityCheck:
        if (!this.checkConnectivity("vec.home://launch", true)) {
          showNetworkWarning();
        }
        break;
      default:
        logger.info("Unknown button pressed");
    }

  }

  private void showNetworkWarning() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);

    // 2. Chain together various setter methods to set the dialog characteristics
    builder.setMessage("No network connection detected. Check cables are connected.").setTitle("Network Connection Error")
        .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {

          public void onClick(DialogInterface dialog, int id) {

          }
        });
    // 3. Get the AlertDialog from create()
    AlertDialog dialog = builder.create();
    dialog.show();

  }

  /**
   * Creates a new instance of VECHome
   */
  @Override
  public void onCreate(Bundle icicle) {
    super.onCreate(icicle);

    stopService(new Intent(VECHome.this, FloatingService.class));
    logger.info("Creating VECHome");
    this.setDefaultKeyMode(DEFAULT_KEYS_SEARCH_LOCAL);

    this.setContentView(R.layout.activity_main);

    View decorView = this.getWindow().getDecorView();
    logger.info("Hiding status bar and setting click listeners for buttons");
    // Hide the status bar.
    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
    decorView.setSystemUiVisibility(uiOptions);

    // Load stagingconfig and retrieve a launchURL if it is set
    JSONObject conf = this.readJSONFile(new File(STAGING_CONFIG_FILE));
    this.launchURL = conf.optString(LAUNCH_URL, null);
    logger.info("LaunchURL set to: {}", this.launchURL);
    try {
      this.heartbeatURL = conf.getString(HEARTBEAT_URL);
      this.connCheckURL = conf.getString(CONN_CHECK_URL);
      this.f5LaunchURL = conf.getString(F5_LAUNCH_URL);
    }
    catch (JSONException e) {
      logger.error("JSON Exception when loading config", e);
    }

    // Set the button listeners
    ((Button) this.findViewById(R.id.launchTraining)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.launchVVote)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.launchClientUI)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.forceBundleCheck)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.restartService)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.callibrateWebCam)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.sendHeartbeat)).setOnClickListener(this);
    ((Button) this.findViewById(R.id.connectivityCheck)).setOnClickListener(this);

    // Remember that you should never show the action bar if the
    // status bar is hidden, so hide that too if necessary.
    ActionBar actionBar = this.getActionBar();
    actionBar.hide();
    this.setDefaultWallpaper();

    // Check the battery level and update
    this.checkBatteryLevel();
    IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

    // Create a new BatteryLevelReceiver - we user an inner class because it updates and calls methods in VECHome to update the
    // underlying data
    batteryLevelReceiver = new BroadcastReceiver() {

      @Override
      public void onReceive(Context context, Intent batteryStatus) {
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        VECHome.this.battPerc = (level / (float) scale) * 100;
        // Update the progress bar
        ProgressBar currentChargePB = (ProgressBar) VECHome.this.findViewById(R.id.currentCharge);
        currentChargePB.setProgress(Math.round(VECHome.this.battPerc));
        ((TextView) VECHome.this.findViewById(R.id.currentChargeText)).setText("Current Charge:"
            + String.valueOf(VECHome.this.battPerc));
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        VECHome.this.updateChargeInfo(status, chargePlug);
      }
    };
    this.registerReceiver(batteryLevelReceiver, batteryLevelFilter);

    // Create a new HeadetReceiver to monitor headphone socket
    IntentFilter headsetFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
    headsetReceiver = new BroadcastReceiver() {

      @Override
      public void onReceive(Context context, Intent headetStatus) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        ((CheckBox) VECHome.this.findViewById(R.id.headsetCheckbox)).setChecked(am.isWiredHeadsetOn());// This reports deprecated,
                                                                                                       // but the api
        // states it is fine for checking if a
        // headset is connected, which is all we
        // are doing
      }
    };
    this.registerReceiver(headsetReceiver, headsetFilter);

    // Update immediately - otherwise we have to wait for a change
    AudioManager am = (AudioManager) this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
    ((CheckBox) this.findViewById(R.id.headsetCheckbox)).setChecked(am.isWiredHeadsetOn());

    // Register for DownloadBundle events
    IntentFilter filter = new IntentFilter(DownloadBundleService.PROCESS_RESPONSE);
    filter.addCategory(Intent.CATEGORY_DEFAULT);
    receiver = new DownloadBundleReceiver();
    this.registerReceiver(receiver, filter);

    // Check which client is installed and adjust the UI accordingly and set the clientService strings
    if (this.isTrainingModeInstalled()) {
      logger.info("Training mode is installed");
      ((Button) this.findViewById(R.id.launchTraining)).setVisibility(View.VISIBLE);
      ((Button) this.findViewById(R.id.launchVVote)).setVisibility(View.GONE);
      ((TextView) this.findViewById(R.id.mode)).setText(Html.fromHtml("Current Mode:\t<font color=\"#333333\">Training</font>"));
    }
    else {
      logger.info("VECClient is installed");
      ((Button) this.findViewById(R.id.launchTraining)).setVisibility(View.GONE);
      ((Button) this.findViewById(R.id.launchVVote)).setVisibility(View.VISIBLE);
      ((TextView) this.findViewById(R.id.mode)).setText(Html.fromHtml("Current Mode:\t<font color=\"#333333\">Live</font>"));
      this.updateSystemStatus();

    }

    this.vVoteTrainingReceiver = new VVoteServiceReceiver();
    filter = new IntentFilter("uk.ac.surrey.cs.tvs.vvote.training.PROCESS_RESPONSE");
    filter.addCategory(Intent.CATEGORY_DEFAULT);
    this.registerReceiver(this.vVoteTrainingReceiver, filter);

    this.vVoteReceiver = new VVoteServiceReceiver();
    filter = new IntentFilter("uk.ac.surrey.cs.tvs.vvote.client.PROCESS_RESPONSE");
    filter.addCategory(Intent.CATEGORY_DEFAULT);
    this.registerReceiver(this.vVoteReceiver, filter);

    logger.info("Get the download bundle status via the client service {},{}", this.clientServicePackage, this.clientService);
    Intent msgIntent = new Intent(VECHome.this, DownloadBundleService.class);
    msgIntent.putExtra(DownloadBundleService.REQUEST_STRING, DownloadBundleService.REQUEST_ALL_INFO);
    this.startService(msgIntent);

    // Restart the actual service (not just getting status updates)
    this.restartService(false);

    logger.info("Get the client status via the client service {},{}", this.clientServicePackage, this.clientService);
    msgIntent = new Intent();

    msgIntent.setComponent(new ComponentName(this.clientServicePackage, this.clientService));
    msgIntent.putExtra("vVoteServiceRequest", "status");
    this.startService(msgIntent);

    logger.info("Disabling Status bar");
    this.disableStatusBar();

    IntentFilter connectivityFilter = new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION);

    // Create a new BatteryLevelReceiver - we user an inner class because it updates and calls methods in VECHome to update the
    // underlying data
    connectivityReceiver = new BroadcastReceiver() {

      @Override
      public void onReceive(Context context, Intent connectivityStatus) {
        boolean noConnectivity = connectivityStatus.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        ((CheckBox) VECHome.this.findViewById(R.id.acNetworkConnected)).setChecked(!noConnectivity);
      }
    };
    this.registerReceiver(connectivityReceiver, connectivityFilter);

    this.checkConnectivity("vec.home://launch", true);

  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (this.vVoteReceiver != null) {
      this.unregisterReceiver(this.vVoteReceiver);
    }
    if (this.vVoteTrainingReceiver != null) {
      this.unregisterReceiver(this.vVoteTrainingReceiver);
    }
    if (this.batteryLevelReceiver != null) {
      this.unregisterReceiver(this.batteryLevelReceiver);
    }
    if (this.headsetReceiver != null) {
      this.unregisterReceiver(this.headsetReceiver);
    }
    if (this.receiver != null) {
      this.unregisterReceiver(this.receiver);
    }
    if (this.connectivityReceiver != null) {
      this.unregisterReceiver(this.connectivityReceiver);
    }
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    // Close the menu
    if (Intent.ACTION_MAIN.equals(intent.getAction())) {
      this.getWindow().closeAllPanels();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    stopService(new Intent(VECHome.this, FloatingService.class));
    this.disableStatusBar();
  }

  /**
   * Root method that attempts to uninstall trainingmode once the download has been successfully downloaded
   * 
   * @return boolean true if successfully uninstalled, false otherwise
   */
  private boolean removeTrainingMode() {

    final PackageManager pm = this.getPackageManager();

    // get a list of installed apps.
    List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
    PackageDeleteObserver observerdelete = new PackageDeleteObserver();
    for (ApplicationInfo packageInfo : packages) {
      if (packageInfo.packageName.equalsIgnoreCase("uk.ac.surrey.cs.tvs.vvote.training")) {

        logger.info("Training mode found in installed apps, showing launch training mode button and hiding launch vVote button");
        try {
          Class<?>[] uninstalltypes = new Class[] { String.class, IPackageDeleteObserver.class, int.class };
          Method uninstallmethod = pm.getClass().getMethod("deletePackage", uninstalltypes);
          uninstallmethod.invoke(pm, new Object[] { packageInfo.packageName, observerdelete, 0 });
        }
        catch (Exception e) {
          logger.error("Exception whilst trying to invoke uninstall", e);
        }
        logger.info("Completed Uninstall");
        return true;
      }
    }
    logger.info("Did not find training mode, cannot uninstall");
    return false;
  }

  /**
   * Wrapper method to restart the relevant client app (training or vvote).
   * 
   * @param stopFirst
   *          boolean if true will shutdown the service first
   */
  private void restartService(boolean stopFirst) {
    logger.info("Restart service request: stopFirst:{}", stopFirst);
    if (this.isTrainingModeInstalled()) {
      logger.info("TrainingMode is installed, will restart TrainingMode");
      Intent restartIntent = new Intent();
      restartIntent.setComponent(new ComponentName("uk.ac.surrey.cs.tvs.vvote.training",
          "uk.ac.surrey.cs.tvs.vvote.training.VVoteTrainingService"));
      if (stopFirst) {
        logger.info("Stopping training mode");
        this.stopService(restartIntent);
      }
      logger.info("Starting training mode");
      this.startService(restartIntent);

    }
    else {
      logger.info("Will restart vvoteclient");
      Intent restartIntent = new Intent();
      restartIntent.setComponent(new ComponentName("uk.ac.surrey.cs.tvs.vvote.client",
          "uk.ac.surrey.cs.tvs.vvote.client.VVoteService"));
      if (stopFirst) {
        logger.info("Stopping vvoteclient");
        this.stopService(restartIntent);
      }
      logger.info("Starting vvoteclient");
      this.startService(restartIntent);
    }
  }

  /**
   * When no wallpaper was manually set, a default wallpaper is used instead.
   */
  private void setDefaultWallpaper() {
    Drawable wallpaper = WallpaperManager.getInstance(this).getDrawable();
    this.getWindow().setBackgroundDrawable(new ClippedDrawable(wallpaper));

  }

  /**
   * Launches the connectivity check async task
   * 
   * @param postLaunchURL
   *          String of the URL to goto following a successful check/reconnect
   * @param showHome
   *          boolean of whether the floating notification should be shown
   * @return boolean false if no network is available, true if a check is being performed - note: a true from here does not indicate
   *         full connectivity, just that a network is available
   */
  private boolean checkConnectivity(String postLaunchURL, boolean showHome) {
    ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    if (!isConnected) {
      ((CheckBox) VECHome.this.findViewById(R.id.acNetworkConnected)).setChecked(false);
      return false;
    }
    try {
      (new ConnectivityCheck(this, postLaunchURL, showHome)).execute(new URL(this.connCheckURL));
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return true;
  }

  /**
   * Callback method to receive the connectivity check result
   * 
   * @param result
   *          boolean of the result, true if successful, false if not
   * @param reconnect
   *          boolean indicating if a reconnect should be tried if the connection failed
   * @param postLaunchURL
   *          String of the URL to call following a successful check
   * @param showHome
   *          boolean of whether the floating notification should be shown
   */
  public void connectivityResult(boolean result, boolean reconnect, String postLaunchURL, boolean showHome) {
    if (result == false && reconnect) {
      if (showHome) {
        startService(new Intent(VECHome.this, FloatingService.class));
      }
      this.cleanUpBrowser();
      try {
        // logger.info("Send Heartbeat launch requested");

        String url = this.f5LaunchURL + URLEncoder.encode(postLaunchURL, "UTF-8");
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setComponent(new ComponentName("org.mozilla.firefox", "org.mozilla.firefox.App"));
        intent.setAction("org.mozilla.gecko.BOOKMARK");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("args", "--url=" + url);
        intent.setData(Uri.parse(url));
        this.disableStatusBar();
        this.startActivity(intent);
      }
      catch (IOException e) {
        logger.error("Exception encoding URL");
      }
    }
    else if (result == true && postLaunchURL != null && !postLaunchURL.equals("vec.home://launch")) {
      // vec.home://launch"
      if (showHome) {
        startService(new Intent(VECHome.this, FloatingService.class));
      }
      this.cleanUpBrowser();
      // logger.info("Send Heartbeat launch requested");
      Intent intent = new Intent(Intent.ACTION_MAIN, null);
      intent.addCategory(Intent.CATEGORY_LAUNCHER);
      intent.setComponent(new ComponentName("org.mozilla.firefox", "org.mozilla.firefox.App"));
      intent.setAction("org.mozilla.gecko.BOOKMARK");
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intent.putExtra("args", "--url=" + postLaunchURL);
      intent.setData(Uri.parse(postLaunchURL));
      this.disableStatusBar();
      this.startActivity(intent);
    }
    ((CheckBox) VECHome.this.findViewById(R.id.acF5Network)).setChecked(result);

  }

  /**
   * Updates the displayed charge info and data about the type of power connection
   * 
   * @param status
   *          int status of battery
   * @param chargePlug
   *          int type of charging plug
   */
  public void updateChargeInfo(int status, int chargePlug) {
    // Are we charging / charged?
    boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
    ((CheckBox) this.findViewById(R.id.chargingCheckbox)).setChecked(isCharging);
    // How are we charging?
    boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
    ((CheckBox) this.findViewById(R.id.usbChargingCheckbox)).setChecked(usbCharge);
    boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
    ((CheckBox) this.findViewById(R.id.acChargingCheckbox)).setChecked(acCharge);

  }

  /**
   * Utility method to update the overall system status having downloaded a bundle. It loads the version.json and displays it on the
   * Home screen
   */
  private void updateSystemStatus() {
    File versionFile = new File(Environment.getExternalStorageDirectory().getPath() + "/vvoteclient/vvwww/bundle/version.json");
    JSONObject bundleVersion = this.readJSONFile(versionFile);
    ((TextView) this.findViewById(R.id.eventName)).setText(Html.fromHtml("Event Name:\t\t<font color=\"#333333\">"
        + bundleVersion.optString("EventName") + "</font>"));
    ((TextView) this.findViewById(R.id.environment)).setText(Html.fromHtml("Environment:\t\t<font color=\"#333333\">"
        + bundleVersion.optString("Environment") + "</font>"));
    ((TextView) this.findViewById(R.id.build)).setText(Html.fromHtml("Build:\t\t\t\t\t\t\t\t<font color=\"#333333\">"
        + bundleVersion.optString("Build") + "</font>"));
    ((TextView) this.findViewById(R.id.revision)).setText(Html.fromHtml("Revision:\t\t\t\t\t<font color=\"#333333\">"
        + bundleVersion.optString("Revision") + "</font>"));

    String dateTime = bundleVersion.optString("DateTime", "");
    if (!dateTime.isEmpty()) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
      Date myDate = null;
      try {
        myDate = dateFormat.parse(dateTime);
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String finalDate = timeFormat.format(myDate);
        ((TextView) this.findViewById(R.id.datetime)).setText(Html.fromHtml("DateTime:\t\t\t\t<font color=\"#333333\">" + finalDate
            + "</font>"));
      }
      catch (ParseException e) {
        e.printStackTrace();
      }

    }

    versionFile = new File(Environment.getExternalStorageDirectory().getPath() + "/vvoteclient/vvwww/data/version.json");
    JSONObject dataVersion = this.readJSONFile(versionFile);
    ((TextView) this.findViewById(R.id.system)).setText(Html.fromHtml("System:\t\t\t\t\t\t<font color=\"#333333\">"
        + dataVersion.optString("Version") + "</font>"));
    ((TextView) this.findViewById(R.id.vechome)).setText(Html.fromHtml("VECHome:\t\t\t\t<font color=\"#333333\">"
        + this.getAppVersion("uk.ac.surrey.cs.tvs.vec.android") + "</font>"));
    ((TextView) this.findViewById(R.id.vecclient)).setText(Html.fromHtml("vVoteClient:\t\t\t<font color=\"#333333\">"
        + this.getAppVersion("uk.ac.surrey.cs.tvs.vvote.client") + "</font>"));
    ((TextView) this.findViewById(R.id.vvotetraining)).setText(Html.fromHtml("vVoteTraining:\t<font color=\"#333333\">"
        + this.getAppVersion("uk.ac.surrey.cs.tvs.vvote.training") + "</font>"));

  }

  /**
   * Gets the version of an app from the package manager
   * 
   * @param appPackage
   *          String app package to look up
   * @return String of version info or Not Installed if not found
   */
  private String getAppVersion(String appPackage) {
    StringBuffer sb = new StringBuffer();
    PackageInfo packageInfo;
    try {
      packageInfo = this.getPackageManager().getPackageInfo(appPackage, 0);

      sb.append(packageInfo.versionName);
      sb.append(" Ver:");
      sb.append(packageInfo.versionCode);

    }
    catch (NameNotFoundException e) {
      sb.append("Not Installed");

    }
    return sb.toString();
  }

  /**
   * Utility method for reading a JSONObject form a JSON File
   * 
   * @param file
   *          File to load
   * @return JSONObject containing the contents of the file
   */
  private JSONObject readJSONFile(File file) {
    if (file.exists()) {
      logger.info("Loading version.json");
      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuffer sb = new StringBuffer();
        while ((line = br.readLine()) != null) {
          sb.append(line);
        }
        return new JSONObject(sb.toString());
      }
      catch (JSONException e) {
        logger.warn("Exception reading version.json", e);
      }
      catch (IOException e) {
        logger.warn("Exception reading version.json", e);
      }
      finally {
        if (br != null) {
          try {
            br.close();
          }
          catch (IOException e) {
            logger.warn("Exception closing version.json", e);
          }
        }
      }
      return new JSONObject();
    }
    else {
      logger.warn("Could not find version.json");
      return new JSONObject();
    }
  }
}
