/**
 * Call to get the barcode 
 */
function getBarcode(){
	document.getElementById("serialNoCell").innerHTML="";
	document.getElementById("districtCell").innerHTML="";
	document.getElementById("signatureCell").innerHTML="";
	document.getElementById("checkResult").innerHTML="";
	document.getElementById("barcodeTable").style.backgroundColor="white";
	document.getElementById("barcodeTable").style.color="black";
	window.JSInterface.scanBarcode();
}
function barcodeResult(result){
	if(result=="null"){
		document.getElementById("checkResult").innerHTML="No Barcode Found";
	}else{
		var tokens =result.split(";");		
		document.getElementById("serialNoCell").innerHTML=tokens[2];
		document.getElementById("districtCell").innerHTML=tokens[0];
		document.getElementById("signatureCell").innerHTML=tokens[4];
		var valid =window.JSInterface.checkSignature(tokens[2]+tokens[0],tokens[4]);
		
		
	}
}
function sigCheckResult(valid){
	if(valid){
		document.getElementById("checkResult").innerHTML="Valid";
		document.getElementById("barcodeTable").style.backgroundColor="green";
		document.getElementById("barcodeTable").style.color="black";
	}else{
		document.getElementById("checkResult").innerHTML="INVALID";
		document.getElementById("barcodeTable").style.backgroundColor="red";
		document.getElementById("barcodeTable").style.color="white";
	}
}