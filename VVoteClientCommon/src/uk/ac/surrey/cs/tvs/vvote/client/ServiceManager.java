/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.android.client.R;
import android.app.Activity;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * This is the activity that provides the UI for starting and stopping the VVoteService. In time this will provide further
 * diagnostic and status information. Currently it just provides and start and stop button
 * 
 * @author Chris Culnane
 * 
 */
public abstract class ServiceManager extends Activity implements OnClickListener {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ServiceManager.class);

  /**
   * Buttons to start and stop the service
   */
  private Button              buttonStart, buttonStop;

  /**
   * Handles the onClick of the button
   */
  @Override
  public abstract void onClick(View src);

  /**
   * Create the ServiceManager activity and initialises the buttons
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    logger.info("Creating new Service UI Component");
    this.setContentView(R.layout.activity_vvote_manager);
    this.buttonStart = (Button) this.findViewById(R.id.buttonStart);
    this.buttonStop = (Button) this.findViewById(R.id.buttonStop);
    this.buttonStart.setOnClickListener(this);
    this.buttonStop.setOnClickListener(this);

  }



}
