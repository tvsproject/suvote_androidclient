/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.vvote.client.webcam.CameraNotAttachedException;
import uk.ac.surrey.cs.tvs.vvote.client.webcam.WebCam;
import uk.ac.surrey.cs.tvs.vvote.client.webcam.WebCamServiceConnection;
import android.content.Context;
import android.content.Intent;

import com.ford.openxc.webcam.WebcamManager;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet for reading QRCode from an external webcam and returning the results to the client
 * 
 * @author Chris Culnane
 * 
 */
public class BarcodeServlet implements NanoServlet {

  /**
   * Reference to the underlying service
   */
  private VVoteServiceInterface service;

  /**
   * Reference to the WebCam Object
   */
  private WebCam                webCam         = new WebCam();

  /**
   * Logger
   */
  private static final Logger   logger         = LoggerFactory.getLogger(BarcodeServlet.class);

  /**
   * boolean to store whether the camera is detected
   */
  private boolean               cameraDetected = false;

  /**
   * Tag to use in logcat messages
   */
  private static String         TAG            = "BarcodeServlet";

  /**
   * Constructs a new BarcodeServlet for the specified service
   * 
   * @param service
   *          Service creating this servlet
   */
  public BarcodeServlet(VVoteServiceInterface service) {
    this.service = service;
    logger.info("Created new BarcodeServlet");
  }

  /**
   * Checks if the camera is detected
   * 
   * @return boolean true if the camera is detected, false if not
   */
  public boolean cameraDetected() {
    return this.cameraDetected;
  }

  /**
   * Handles an HTTP request for a barcode
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    WebcamManager mWebcamManager = null;
    Object mServiceSyncToken = new Object();
    WebCamServiceConnection webCamServiceConnection = new WebCamServiceConnection(mServiceSyncToken);

    //logger.info("Request for barcode received");
    //logger.info("Binding to Webcam Service");

    this.service.getService().bindService(new Intent(this.service.getService(), WebcamManager.class), webCamServiceConnection,
        Context.BIND_AUTO_CREATE);
    String barcodeResult = null;
    try {
      synchronized (mServiceSyncToken) {
        if (mWebcamManager == null) {
          //logger.info("WebcamManager is null, will attempt to connect");
          mServiceSyncToken.wait(this.service.getConfig().getIntParameter(AndroidConstants.Config.BARCODE_TIMEOUT));
        }
        mWebcamManager = webCamServiceConnection.getWebCamManager();
        if (mWebcamManager == null) {
          //logger.warn("Couldn't connect to WebCam");
          this.updateCameraStatus(false);
          return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Could not connect to WebCam");
        }
        //logger.info("Calling getBarcode");
        this.updateCameraStatus(true);
        barcodeResult = this.webCam.getBarcode(this.service.getConfig().getIntParameter(AndroidConstants.Config.BARCODE_TIMEOUT),
            mWebcamManager);

      }
    }
    catch (InterruptedException e) {
      this.updateCameraStatus(false);
      logger.warn("Interrupted whilst waiting for WebCam Service");
    }
    catch (CameraNotAttachedException e) {
      this.updateCameraStatus(false);
      //logger.error("No barcode found");
      return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Could not connect to WebCam");
    }
    finally {
      if (mWebcamManager != null) {
        //Log.i(TAG, "Unbinding from webcam manager");
        this.service.getService().unbindService(webCamServiceConnection);
        //logger.info("Unbinding from the webcam");
        mWebcamManager = null;
      }
    }
    if (barcodeResult != null) {
      // NOTE: DO NOT LOG BARCODE CONTENTS - COULD BE SECRET
      //logger.info("Barcode found");
      return new Response(Response.Status.OK, NanoHTTPD.MIME_PLAINTEXT, barcodeResult);
    }
    else {
      //logger.info("No barcode found");
      return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "No Barcode Found");
    }
  }

  /**
   * Updates the camera status - for example, following a disconnection
   * 
   * @param val
   *          boolean true if camera is detected, false if not
   */
  private void updateCameraStatus(boolean val) {
    if (this.cameraDetected != val) {
      this.cameraDetected = val;
      this.service.sendStatus();
    }
  }

}
