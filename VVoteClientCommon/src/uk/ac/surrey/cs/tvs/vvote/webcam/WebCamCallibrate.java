/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.webcam;

import uk.ac.surrey.cs.tvs.android.client.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * A simple utility activity to provide an easy way of calibrating the location of the webcam in the voting booth. This just streams
 * the webcam feed to the screen
 * 
 * @author Chris Culnane
 * 
 */
public class WebCamCallibrate extends Activity implements OnClickListener {

  /**
   * handles the onclick, only accepted event is the exit button
   */
  @Override
  public void onClick(View src) {
    if (src.getId() == R.id.exitButton) {
      this.finish();

    }

  }

  /**
   * Creates the activity from the layout and adds a listener to the button
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.cameracallibrate);
    ((Button) this.findViewById(R.id.exitButton)).setOnClickListener(this);

  }

}
