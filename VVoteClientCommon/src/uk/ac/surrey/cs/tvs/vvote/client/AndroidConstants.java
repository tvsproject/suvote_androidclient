/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import android.os.Environment;

/**
 * Class containing static constants for referencing the various config values in the android config file
 * 
 * @author Chris Culnane
 * 
 */
public class AndroidConstants {

  /**
   * Constants for accessing data in the config files
   * 
   * @author Chris Culnane
   * 
   */
  public static class Config {

    /**
     * root server folder property
     */
    public static final String SERVER_ROOT     = "serverRoot";
    /**
     * server port property
     */
    public static final String SERVER_PORT     = "serverPort";
    /**
     * sets if printing is in debug - do not use will save images and impact on performance
     */
    public static final String PRINT_DEBUG     = "printDebug";

    /**
     * debug directory property - where to save debug images
     */
    public static final String DEBUG_DIR       = "debugDir";

    /**
     * printer buffer property - size of buffer
     */
    public static final String PRINT_BUFFER    = "printerBuffer";

    /**
     * timeout for looking for a barcode property
     */
    public static final String BARCODE_TIMEOUT = "barcodeTimeout";

  }

  /**
   * Paths constants for location of various config files
   * 
   * @author Chris Culnane
   * 
   */
  public static class Paths {

    /**
     * App folder - sd card and vvoteclient
     */
    public static final String APP_FOLDER      = Environment.getExternalStorageDirectory().getAbsolutePath() + "/vvoteclient/";
    /**
     * Config file path
     */
    public static final String CONFIG_FILE     = APP_FOLDER + "config.json";
    /**
     * Client config path
     */
    public static final String CLIENT_CONFIG   = APP_FOLDER + "client.conf";

    /**
     * District config path
     */
    public static final String DISTRICT_CONFIG = APP_FOLDER + "districtconf.json";

  }

  /**
   * Loads native libraries GMP and PBC
   */
  public static void loadLibrary() {
    System.loadLibrary("gmp");
    System.loadLibrary("pbc");

  }
}
