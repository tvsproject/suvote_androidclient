/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.IOException;
import java.io.OutputStream;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * EpsonPrinter object to handle communicating with the EpsonPrinter. This should be generic, in that it should be able to
 * communicate via either USB or network. Currently we only support USB connection, but there should be nothing in here that cannot
 * be made to work over a network connection, should we wish to switch to that at some point in the future.
 * 
 * @author Chris Culnane
 * 
 */
public class EpsonPrinter {

  /**
   * Connection type enum, USB and NETWORK are provided, but support is only available for USB
   * 
   * @author Chris Culnane
   * 
   */
  public static enum ConnectionType {
    USB, NETWORK
  }

  /**
   * Sets a bit within a byte to either 1 or 0. TODO expand on this
   * 
   * @param data
   *          byte array holding the byte we are setting
   * @param pos
   *          the position in the byte array of the individual byte we are setting
   * @param val
   *          with the bit should be 1 or 0
   */
  private static void setBit(byte[] data, int pos, int val) {
    int posByte = pos / 8;
    int posbit = pos % 8;
    byte oldbyte = data[posByte];
    oldbyte = (byte) (((0xFF7F >> posbit) & oldbyte) & 0x00FF);
    byte newbyte = (byte) ((val << (8 - (posbit + 1))) | oldbyte);
    data[posByte] = newbyte;
  }

  /**
   * EpsonThermalPrinter variable that wraps any kinds of EpsonThermalPrinter
   */
  private EpsonThermalPrinter etp;

  /**
   * Connection type for this instance (currently only USB is supported)
   */
  private ConnectionType      type;

  /**
   * Constant used for image processing
   */
  private static final int    MAX_IMAGE_HEIGHT = 2303;

  /**
   * Create a new EpsonPrinter with the specified Connection type, using the passed in context. This will construct a new generic
   * USBPrinter object that will perform the actual connection operation. Currently the only connection type supported is USB. If an
   * alternative type is set an exception will be thrown.
   * 
   * @param type
   *          ConnetionType currently only USB
   * @param ctx
   *          Context we are running this in
   */
  public EpsonPrinter(ConnectionType type, Context ctx) {
    this.type = type;
    switch (this.type) {
      case USB:
        this.etp = new EpsonUSBPrinter(ctx);
        break;
      case NETWORK:
        throw new UnsupportedOperationException("Not yet implemented");
    }
  }

  /**
   * Closes any open connections to the printer - once called this printer cannot be used again a new instance will need to be
   * created
   * 
   * @throws IOException
   */
  public void close() throws IOException {
    this.etp.close();
  }

  /**
   * Gets the underlying outputstream for communicating directly with the printer
   * 
   * @return OutputStream that sends data to the printer
   */
  public OutputStream getOutputStream() {
    return this.etp.getOutputStream();
  }

  /**
   * Checks whether this printer is ready - this will only return true if the connection has been setup
   * 
   * @return boolean true if the printer has been found and a connection created
   */
  public boolean isPrinterReady() {
    return this.etp.isReady();
  }

  /**
   * Print an image to the printer
   * 
   * @param img
   *          BitMap image that needs printing
   * @throws IOException
   */
  public void printImage(Bitmap img) throws IOException {
    try {
      // We can only print full rows of 32 pixels - so we need the image to be divisible by 32. if it
      // isn't we pad with a border either side
      int image_border = img.getWidth() % 32;
      int[] border = new int[2];
      // The image is exactly divisible by 32, no padding needed
      if (image_border == 0) {
        border[0] = 0;
        border[1] = 0;
      }
      else {
        // padding is needed, work out how much overall
        image_border = 32 - (img.getWidth() % 32);
        // Check if the padding is divisible by 2, if it is share it evenly each side
        if ((image_border % 2) == 0) {
          border[0] = image_border / 2;
          border[1] = image_border / 2;
        }
        else {
          // if it isn't we pad half to the left and half + 1 to the right (int division takes the floor of the value, so we know we
          // need to add 1 to the right hand side)
          border[0] = image_border / 2;
          border[1] = (image_border / 2) + 1;
        }
      }
      // Get the image height - we can only print in pages of 2303, we can print multiple pages, but need to perform the division
      // ourselves
      int remainingHeight = img.getHeight();
      // Prepare an array for the row data. This will consist of padding bytes and the image width, divided by 8 to get the number
      // of
      // bytes - the padding ensures this division works correctly
      byte[] rowdata = new byte[(border[0] + border[1] + img.getWidth()) / 8];
      // We will step through the image in segments (pages)
      for (int imgSeg = 0; imgSeg < (img.getHeight() / MAX_IMAGE_HEIGHT) + 1; imgSeg++) {
        // Check if we have more than a single segment left to print
        if (remainingHeight > MAX_IMAGE_HEIGHT) {
          // we do have more than single page - tell the printer to expect raster data
          this.etp.getOutputStream().write(ESCPOS.S_RASTER_N);
          // calculate how much data we will send - this is the max size of a segment since we have >1 segment to print
          byte[] sizeData = new byte[] { (byte) ((img.getWidth() + border[0] + border[1]) / 8), (byte) (0),
              (byte) (MAX_IMAGE_HEIGHT % 256), (byte) (MAX_IMAGE_HEIGHT / 256) };
          // tell the printer how much we will send
          this.etp.getOutputStream().write(sizeData);
        }
        else {
          // We only have 1 more segment to print, tell the printer to expect raster data
          this.etp.getOutputStream().write(ESCPOS.S_RASTER_N);
          // Calculate size of data from remaining height
          byte[] sizeData = new byte[] { (byte) ((img.getWidth() + border[0] + border[1]) / 8), (byte) (0),
              (byte) (remainingHeight % 256), (byte) (remainingHeight / 256) };
          // tell the printer how much data we will send
          this.etp.getOutputStream().write(sizeData);
        }

        // Loop through rows in the image till we hit the max segment height or end of image
        for (int y = 0; y < MAX_IMAGE_HEIGHT && y < remainingHeight; y++) {
          int colIndex = 0;
          // set the overall image location based on number of segments already printed
          int currentY = y + (imgSeg * MAX_IMAGE_HEIGHT);

          // Set the left border blank pixel bits
          for (int b = 0; b < border[0]; b++) {
            setBit(rowdata, colIndex, 0);
            colIndex++;
          }
          // Loop through the pixels in this row and if they are black set the bit to 1, otherwise set it to 0
          for (int x = 0; x < img.getWidth(); x++) {
            if ((img.getPixel(x, currentY) & 0x80) >> 7 == 0) {
              setBit(rowdata, colIndex, 1);
              colIndex++;
            }
            else {
              setBit(rowdata, colIndex, 0);
              colIndex++;
            }
          }
          // Set the right border blank pixel bits
          for (int b = 0; b < border[1]; b++) {
            setBit(rowdata, colIndex, 0);
            colIndex++;
          }
          // Write the actual row of data to the stream
          this.etp.getOutputStream().write(rowdata);
        }
        // calculate the new remaining height
        remainingHeight = remainingHeight - MAX_IMAGE_HEIGHT;
      }
      // finally flush the stream
      this.etp.getOutputStream().flush();
    }
    catch (IOException e) {
      this.etp.setIsReady(false);
      throw e;
    }
  }

  /**
   * Sets the buffer size of the underlying communication medium. This is relevant for USB printers, but may not be for network
   * printers
   * 
   * @param value
   *          int buffer size
   */
  public void setBuffer(int value) {
    this.etp.setBuffer(value);
  }

}
