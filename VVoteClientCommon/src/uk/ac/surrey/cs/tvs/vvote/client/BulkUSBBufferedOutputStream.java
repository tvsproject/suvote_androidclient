/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;

/**
 * Provides a wrapper of the underlying USB communication streams. It provides a way of buffering the data into the appropriate
 * sized blocks for the underlying USB connection
 * 
 * @author Chris Culnane
 * 
 */
public class BulkUSBBufferedOutputStream extends ByteArrayOutputStream {

  /**
   * Logger
   */
  private static final Logger logger     = LoggerFactory.getLogger(BulkUSBBufferedOutputStream.class);
  /**
   * USB Device Connection to use
   */
  private UsbDeviceConnection connection;

  /**
   * USB EndPoint we are sending to
   */
  private UsbEndpoint         endpoint;

  /**
   * The byte buffer size
   */
  private int                 bufferSize = 16;

  /**
   * Constructs a new BulkUSBBufferedOutputStream with the default sized buffer
   * 
   * @param connection
   *          UsbDeviceConnection to use for communicating
   * @param endpoint
   *          UsbEndpoint we are communicating with
   */
  public BulkUSBBufferedOutputStream(UsbDeviceConnection connection, UsbEndpoint endpoint) {
    super();
    logger.info("Created new BulkUSBBufferedOutputStream with buffersize:{}", this.bufferSize);
    this.connection = connection;
    this.endpoint = endpoint;

  }

  /**
   * Constructs a new BulkUSBBufferedOutputStream with an explicitly sized buffer
   * 
   * @param connection
   *          UsbDeviceConnection to use for communicating
   * @param endpoint
   *          UsbEndpoint we are communicating with
   * @param size
   *          int buffer size
   */
  public BulkUSBBufferedOutputStream(UsbDeviceConnection connection, UsbEndpoint endpoint, int size) {
    super(size);
    this.bufferSize = size;
    logger.info("Created new BulkUSBBufferedOutputStream with buffersize:{}", this.bufferSize);
    this.connection = connection;
    this.endpoint = endpoint;
  }

  /**
   * Flushes and then closes the stream
   */
  @Override
  public synchronized void close() throws IOException {
    this.flush();
    this.connection.close();
  }

  /**
   * Flush the stream and send any remaining data even if smaller than the buffer size
   */
  @Override
  public synchronized void flush() throws IOException {
    super.flush();
    this.connection.bulkTransfer(this.endpoint, super.toByteArray(), super.size(), 0);
    super.reset();

  }

  /**
   * Used to set or change the buffer size
   * 
   * @param bufSize
   *          int buffer size to use
   */
  public synchronized void setBufferSize(int bufSize) {
    this.bufferSize = bufSize;
    logger.info("Setting buffersize:{}", this.bufferSize);
  }

  /**
   * Write the data to the stream, the data will be initially cached in the buffer. If the buffered data is larger than the
   * buffersize this will cause a USB bulk transfer as well
   */
  @Override
  public synchronized void write(byte[] buffer, int offset, int length) {
    super.write(buffer, offset, length);
    if (super.size() >= this.bufferSize) {
      this.connection.bulkTransfer(this.endpoint, super.toByteArray(), super.size(), 0);

      super.reset();
    }

  }

  /**
   * Write a single byte to the stream, the data will be initially cached in the buffer. If the buffered data is larger than the
   * buffersize this will cause a USB bulk transfer as well
   */
  @Override
  public synchronized void write(int oneByte) {
    super.write(oneByte);
    if (super.size() >= this.bufferSize) {
      this.connection.bulkTransfer(this.endpoint, super.toByteArray(), super.size(), 0);
      super.reset();
    }
  }

}
