/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_jsexp_precomp(ecn_jsexp_tab table, mpz_t *basesx, mpz_t *basesy,
		  mpz_t *basesz)
{
  int i, j;           /* Index variables. */
  size_t block_width; /* Width of current subtable. */
  size_t tab_len;     /* Size of current subtable. */
  mpz_t *tx;          /* Temporary variable for subtable. */
  mpz_t *ty;          /* Temporary variable for subtable. */
  mpz_t *tz;          /* Temporary variable for subtable. */

  mpz_t t1;           /* Temporary variable for curve operations. */
  mpz_t t2;           /* Temporary variable for curve operations. */
  mpz_t t3;           /* Temporary variable for curve operations. */
  mpz_t U1;           /* Temporary variable for curve operations. */
  mpz_t U2;           /* Temporary variable for curve operations. */
  mpz_t S1;           /* Temporary variable for curve operations. */
  mpz_t S2;           /* Temporary variable for curve operations. */
  mpz_t H;            /* Temporary variable for curve operations. */
  mpz_t r;            /* Temporary variable for curve operations. */

  mpz_t zz;           /* Temporary variable for curve operations. */
  mpz_t zzz;          /* Temporary variable for curve operations. */

  int mask;           /* Mask used for dynamic programming */
  int one_mask;       /* Mask containing a single non-zero bit. */

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(t3);
  mpz_init(U1);
  mpz_init(U2);
  mpz_init(S1);
  mpz_init(S2);
  mpz_init(H);
  mpz_init(r);

  mpz_init(zz);
  mpz_init(zzz);

  block_width = table->block_width;
  tab_len = 1 << block_width;


  for (i = 0; i < table->tabs_len; i++)
    {
      /* Last block may have smaller width. */
      if (i == table->tabs_len - 1)
	{
	  block_width = table->len - (table->tabs_len - 1) * block_width;
	  tab_len = 1 << block_width;
	}

      /* Current subtable. */
      tx = table->tabsx[i];
      ty = table->tabsy[i];
      tz = table->tabsz[i];

      /* Initialize current subtable with all trivial products. */
      mpz_set_si(tx[0], 0);
      mpz_set_si(ty[0], 1);
      mpz_set_si(tz[0], 0);

      mask = 1;
      for (j = 0; j < block_width; j++)
	{
	  mpz_set(tx[mask], basesx[j]);
	  mpz_set(ty[mask], basesy[j]);
	  mpz_set(tz[mask], basesz[j]);
	  mask <<= 1;
	}

      /* Initialize current subtable with all non-trivial products. */
      for (mask = 1; mask < (1 << block_width); mask++)
	{
	  one_mask = mask & (-mask);

	  mpz_set_si(zz, 0);
	  mpz_set_si(zzz, 0);

	  ecn_jmul(t1, t2, t3,
		   U1, U2,
		   S1, S2,
		   H, r,
		   tx[mask], ty[mask], tz[mask],
		   table->modulus, table->a,
		   tx[mask ^ one_mask],
		   ty[mask ^ one_mask],
		   tz[mask ^ one_mask],
		   tx[one_mask],
		   ty[one_mask],
		   tz[one_mask],
		   zz, zzz);
	}

      basesx += block_width;
      basesy += block_width;
      basesz += block_width;
    }

  mpz_clear(t1);
  mpz_clear(t2);
  mpz_clear(t3);
  mpz_clear(U1);
  mpz_clear(U2);
  mpz_clear(S1);
  mpz_clear(S2);
  mpz_clear(H);
  mpz_clear(r);

  mpz_clear(zz);
  mpz_clear(zzz);

}
