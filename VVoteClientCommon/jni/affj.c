/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_affj(mpz_t X, mpz_t Y, mpz_t Z, mpz_t modulus) {

  if (mpz_cmp_si(X, -1) == 0)
    {
      mpz_set_si(X, 0);
      mpz_set_si(Y, 1);
      mpz_set_si(Z, 0);
    }
  else
    {
      mpz_set_si(Z, 1);
    }
}
