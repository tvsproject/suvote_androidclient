# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)
LOCAL_MODULE := libgmp
LOCAL_SRC_FILES := libgmp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libpbc
LOCAL_SRC_FILES := libpbc.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libjnidispatch
LOCAL_SRC_FILES := libjnidispatch.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libjpbc-pbc
LOCAL_SRC_FILES := libjpbc-pbc.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := nativeec
LOCAL_SRC_FILES := nativeec.c convert.c mul.c square.c jexp.c jexp_aff.c ressol.c jsquare.c jmul.c affj.c jaff.c exp.c
LOCAL_SHARED_LIBRARIES := gmp
include $(BUILD_SHARED_LIBRARY)