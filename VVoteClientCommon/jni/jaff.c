/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <gmp.h>
#include "ecn.h"

void
ecn_jaff(mpz_t X, mpz_t Y, mpz_t Z, mpz_t modulus) {

  mpz_t ZZ;

  if (mpz_cmp_si(Z, 0) == 0)
    {
      mpz_set_si(X, -1);
      mpz_set_si(Y, -1);
    }
  else
    {

      mpz_init(ZZ);

      mpz_invert(Z, Z, modulus);

      mpz_mul(ZZ, Z, Z);
      mpz_mod(ZZ, ZZ, modulus);

      mpz_mul(X, X, ZZ);
      mpz_mod(X, X, modulus);

      mpz_mul(Z, ZZ, Z);
      mpz_mod(Z, Z, modulus);

      mpz_mul(Y, Y, Z);
      mpz_mod(Y, Y, modulus);

      mpz_clear(ZZ);
    }
}
