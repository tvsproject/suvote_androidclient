/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <gmp.h>
#include "ecn.h"

void
ecn_jexp_aff(mpz_t rx, mpz_t ry,
	     mpz_t modulus, mpz_t a, mpz_t b,
	     mpz_t x, mpz_t y,
	     mpz_t exponent) {

  mpz_t RZ;

  mpz_t X;
  mpz_t Y;
  mpz_t Z;

  mpz_init(RZ);

  mpz_init(X);
  mpz_init(Y);
  mpz_init(Z);

  mpz_set(X, x);
  mpz_set(Y, y);

  ecn_affj(X, Y, Z, modulus);

  ecn_jexp(rx, ry, RZ,
	   modulus, a,
	   X, Y, Z,
	   exponent);

  ecn_jaff(rx, ry, RZ, modulus);

  mpz_clear(Z);
  mpz_clear(Y);
  mpz_clear(X);

  mpz_clear(RZ);
}
