/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_jsquare_aff(mpz_t t1, mpz_t t2, mpz_t s,
		mpz_t rx, mpz_t ry,
		mpz_t modulus, mpz_t a, mpz_t b,
		mpz_t x, mpz_t y) {

  mpz_t t3;
  mpz_t S;
  mpz_t M;
  mpz_t T;

  mpz_t Z1;
  mpz_t Z3;

  mpz_init(t3);
  mpz_init(S);
  mpz_init(M);
  mpz_init(T);

  mpz_init(Z1);
  mpz_init(Z3);

  ecn_affj(x, y, Z1, modulus);

  ecn_jsquare(t1, t2, t3,
	      S, M, T,
	      rx, ry, Z3,
	      modulus, a,
	      x, y, Z1);

  ecn_jaff(rx, ry, Z3, modulus);

  mpz_clear(Z3);
  mpz_clear(Z1);

  mpz_clear(T);
  mpz_clear(M);
  mpz_clear(S);
  mpz_clear(t3);
}
