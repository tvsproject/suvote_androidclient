#include "nativeec.h"
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "convert.h"
#include "ecn.h"



#define JACOBI_COORDINATES
void mpz_t_pair_to_2DjbyteArray(JNIEnv *env, jobjectArray* res,
				mpz_t first, mpz_t second) {

  jbyteArray firstArray;
  jbyteArray secondArray;

  // Get the byte array class
  jclass byteArrayClass = (*env)->FindClass(env, "[B");

  *res = (*env)->NewObjectArray(env, (jsize)2, byteArrayClass, NULL);

  mpz_t_to_jbyteArray(env, &firstArray, first);
  mpz_t_to_jbyteArray(env, &secondArray, second);

  (*env)->SetObjectArrayElement(env, *res, (jsize)0, firstArray);
  (*env)->SetObjectArrayElement(env, *res, (jsize)1, secondArray);
}

/*
 * Class:     jecn_ECN
 * Method:    mul
 * Signature: ([B[B[B[B[B[B[B)[[B
 */
JNIEXPORT jobjectArray JNICALL Java_uk_surrey_cs_tvs_opt_NativeEC_mul
(JNIEnv *env, jclass clazz,
 jbyteArray javaModulus, jbyteArray javaA, jbyteArray javaB,
 jbyteArray javax1, jbyteArray javay1,
 jbyteArray javax2, jbyteArray javay2) {

  jobjectArray javaResult;

  mpz_t modulus;
  mpz_t a;
  mpz_t b;
  mpz_t x1;
  mpz_t y1;
  mpz_t x2;
  mpz_t y2;
  mpz_t resultx;
  mpz_t resulty;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  /* Convert curve parameters represented as byte[] to mpz_t. */
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
  jbyteArray_to_mpz_t(env, &a, javaA);
  jbyteArray_to_mpz_t(env, &b, javaB);

  /* Convert point coordinates as byte[] to mpz_t */
  jbyteArray_to_mpz_t(env, &x1, javax1);
  jbyteArray_to_mpz_t(env, &y1, javay1);
  jbyteArray_to_mpz_t(env, &x2, javax2);
  jbyteArray_to_mpz_t(env, &y2, javay2);

  mpz_init(resultx);
  mpz_init(resulty);

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(s);

  ecn_mul(t1, t2, s,
	  resultx, resulty,
	  modulus, a, b,
	  x1, y1,
	  x2, y2);

  mpz_t_pair_to_2DjbyteArray(env, &javaResult, resultx, resulty);

  mpz_clear(s);
  mpz_clear(t2);
  mpz_clear(t1);

  mpz_clear(y2);
  mpz_clear(x2);
  mpz_clear(y1);
  mpz_clear(x1);
  mpz_clear(resultx);
  mpz_clear(resulty);
  mpz_clear(b);
  mpz_clear(a);
  mpz_clear(modulus);

  return javaResult;
}


JNIEXPORT jobjectArray JNICALL Java_uk_surrey_cs_tvs_opt_NativeEC_exp
  (JNIEnv *env, jclass clazz, jbyteArray javaModulus, jbyteArray javaA,
   jbyteArray javaB, jbyteArray javaX, jbyteArray javaY,
   jbyteArray javaExponent)
{
	 mpz_t modulus;
	  mpz_t a;
	  mpz_t b;
	  mpz_t x;
	  mpz_t y;
	  mpz_t exponent;

	  mpz_t rx;
	  mpz_t ry;


	  jobjectArray javaResult;

	  /* Translate jbyteArray-parameters to their corresponding GMP
	     mpz_t-elements. */
	  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
	  jbyteArray_to_mpz_t(env, &a, javaA);
	  jbyteArray_to_mpz_t(env, &b, javaB);
	  jbyteArray_to_mpz_t(env, &x, javaX);
	  jbyteArray_to_mpz_t(env, &y, javaY);
	  jbyteArray_to_mpz_t(env, &exponent, javaExponent);

	  /* Compute exponentiation. */

	  mpz_init(rx);
	  mpz_init(ry);

	#ifdef JACOBI_COORDINATES

	  ecn_jexp_aff(rx, ry, modulus, a, b, x, y, exponent);

	#else

	  ecn_exp(rx, ry, modulus, a, b, x, y, exponent);

	#endif

	  mpz_t_pair_to_2DjbyteArray(env, &javaResult, rx, ry);


	  /* Deallocate resources. */
	  mpz_clear(ry);
	  mpz_clear(rx);

	  mpz_clear(exponent);
	  mpz_clear(y);
	  mpz_clear(x);
	  mpz_clear(b);
	  mpz_clear(a);
	  mpz_clear(modulus);

	  return javaResult;
}



