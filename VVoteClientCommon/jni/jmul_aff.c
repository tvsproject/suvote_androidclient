/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_jmul_aff(mpz_t t1, mpz_t t2, mpz_t s,
	     mpz_t rx, mpz_t ry,
	     mpz_t modulus, mpz_t a, mpz_t b,
	     mpz_t x1, mpz_t y1,
	     mpz_t x2, mpz_t y2) {

  mpz_t t3;
  mpz_t U1;
  mpz_t U2;
  mpz_t S1;
  mpz_t S2;
  mpz_t H;
  mpz_t r;

  mpz_t X1;
  mpz_t Y1;
  mpz_t Z1;

  mpz_t X2;
  mpz_t Y2;
  mpz_t Z2;

  mpz_t Z3;

  mpz_t Z2Z2;
  mpz_t Z2Z2Z2;

  mpz_init(t3);
  mpz_init(U1);
  mpz_init(U2);
  mpz_init(S1);
  mpz_init(S2);
  mpz_init(H);
  mpz_init(r);

  mpz_init(X1);
  mpz_init(Y1);
  mpz_init(Z1);

  mpz_init(X2);
  mpz_init(Y2);
  mpz_init(Z2);

  mpz_init(Z3);

  mpz_init(Z2Z2);
  mpz_init(Z2Z2Z2);

  mpz_set(X1, x1);
  mpz_set(Y1, y1);

  mpz_set(X2, x2);
  mpz_set(Y2, y2);

  ecn_affj(X1, Y1, Z1, modulus);
  ecn_affj(X2, Y2, Z2, modulus);

  /* /\* Compute powers of Z2 *\/ */
  /* mpz_mul(Z2Z2, Z2, Z2); */
  /* mpz_mod(Z2Z2, Z2Z2, modulus); */
  /* mpz_mul(Z2Z2Z2, Z2Z2, Z2); */
  /* mpz_mod(Z2Z2Z2, Z2Z2Z2, modulus); */

  ecn_jmul(t1, t2, t3,
	   U1, U2,
	   S1, S2,
	   H,  r,
	   rx, ry, Z3,
	   modulus, a,
	   X1, Y1, Z1,
	   X2, Y2, Z2,
	   Z2Z2, Z2Z2Z2);

  ecn_jaff(rx, ry, Z3, modulus);

  mpz_clear(Z3);

  mpz_init(Y1);
  mpz_init(X1);
  mpz_init(Z1);

  mpz_init(Z2);
  mpz_init(Y2);
  mpz_init(X2);

  mpz_clear(Z2Z2Z2);
  mpz_clear(Z2Z2);

  mpz_clear(r);
  mpz_clear(H);
  mpz_clear(S2);
  mpz_clear(S1);
  mpz_clear(U2);
  mpz_clear(U1);
  mpz_clear(t3);
}
